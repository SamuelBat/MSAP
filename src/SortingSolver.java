import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * 
 *
 * @author Alexis Vernes
 *
 */
public class SortingSolver implements Solver {

	@Override
	public Solution _solve(Set<Table> patch, List<Table> tables, int nbVoisinMax) {
		Solution solution = new Solution();
		// better than sorting at every step
		ArrayList<Table>[] sort = new ArrayList[nbVoisinMax + 1];
		for (int i = 0; i < sort.length; i++) {
			sort[i] = new ArrayList<>();
		}
		HashMap<Integer, Integer> nbVoisins = new HashMap<>();
		patch.forEach(t -> {
			nbVoisins.put(t.id, t.voisins.size());
			sort[t.voisins.size()].add(t);
		});

		boolean[] changed = { false };
		do {
			changed[0] = false; // pointer

			// i is a pointer
			for (int[] i = { 0 }; i[0] < sort.length && !changed[0]; i[0]++) {
				Collections.shuffle(sort[i[0]]);
				while (!sort[i[0]].isEmpty() && !changed[0]) {
					 Table t = sort[i[0]].remove(sort[i[0]].size() - 1);
					if (solution.get(t) == -1) {
						solution.set(t,1); // select
						t.voisins.stream()
								.map(voisin -> tables.get(voisin - 1))
								.filter(tv -> solution.get(tv) == -1)
								.forEach(tv -> {
									solution.set(tv, 0); // deselect neibourg
									tv.voisins.stream()
											.map(voisin -> tables.get(voisin - 1))
											.filter(tvv -> solution.get(tvv) == -1)
											.forEach(tvv -> {
												int nvc = nbVoisins.get(tvv.id) - 1;
												if (nvc < i[0]) {
													changed[0] = true; // jump back to sort[0]
												}
												sort[nvc].add(tvv); // move in sort
												nbVoisins.put(tvv.id, nvc); // update count
											});
								});
					}
				}
			}
		} while (changed[0]);

		return solution;
	}
}
