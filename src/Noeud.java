import java.util.ArrayDeque;

//import jdk.internal.org.jline.reader.impl.history.DefaultHistory;

public class Noeud {
	/***********************
	 * int xmin, xmax, ymin, ymax Table table boolean feuille Node xiyi, xiys, xsyi,
	 * xsys
	 */
	int xmin, xmax, ymin, ymax;
	Table table;
	boolean feuille;
	// int borne;
	Noeud xiyi, xiys, xsyi, xsys;

	/********************/
	/*******************************************
	 * constructeur Node(_xmin, _xmax, _ymin, _ymax) feuille = vrai xmin, xmax,
	 * ymin, ymax = _xmin, _xmax, _ymin, _ymax fin constructeur
	 ******************************************
	 * CONSTRUCTEUR /
	 ******************/
	public Noeud(int xmin, int xmax, int ymin, int ymax) {
		super();
		this.xmin = xmin;
		this.xmax = xmax;
		this.ymin = ymin;
		this.ymax = ymax;
		this.feuille = true;
	}

	/******************************************
	 * fonction borneX() retourner xmax-xmin fin fonction
	 * 
	 * fonction borneY() retourner ymax-ymin fin fonction
	 *************************
	 */
	protected int getBornX() {
		return (xmax - xmin) / 2;
	}

	protected int getBornY() {
		return (ymax - ymin) / 2;
	}

	/******************************************
	 * fonction creerBranches() feuille = faux xiyi = Node(xmin, borneX(), ymin,
	 * borneY()) xiys = Node(xmin, borneX(), borneY(), ymax) xsyi = Node(borneX(),
	 * xmax, ymin, borneY()) xsys = Node(borneX(), xmax, borneY(), ymax) fin
	 * fonction
	 ********************************************/

	protected void createBranche() {
		feuille = false;
		xiyi = new Noeud(xmin, getBornX(), ymin, getBornY());
		xiys = new Noeud(xmin, getBornX(), getBornY(), ymax);
		xsyi = new Noeud(getBornX(), xmax, ymin, getBornY());
		xsys = new Noeud(getBornX(), xmax, getBornY(), ymax);
	}

	/**********************************************
	 * fonction add(Table t) si table non d�fini alors table = t sinon si feuille
	 * alors creerBranches() fin si
	 * 
	 * si t.x < borneX() alors si t.y < borney() alors xiyi.add(t) sinon xiys.add(t)
	 * fin si sinon si t.y < borney() alors xsyi.add(t) sinon xsys.add(t) fin si fin
	 * si fin si fin fonction
	 */
	/************ AJOUT DE TABLE ***************/
	/****************************************/
	public void addTable(Table t) {
		Noeud current = this;
		while (current != null) {
			if (current.table == null) {
				current.table = t;
				current = null;
			} else {
				if (current.feuille == true) {
					current.createBranche();
				}
				if (t.x < current.getBornX()) {
					if (t.y < current.getBornY()) {
						current = current.xiyi;
					} else {
						current = current.xiys;
					}
				} else {
					if (t.y < current.getBornY()) {
						current = current.xsyi;
					} else {
						current = current.xsys;
					}
				}
			}
		}
	}

	protected static long distanceSquare(Table a, Table b) {
		return pow(a.getX() - b.getX()) + pow(a.getY() - b.getY());
	}
	
	/**********************************************
	 * ******************************************** fonction sontVoisin(Table a,
	 * Table b, distance) retourner (a.x-b.x)^2+(a.y-b.y)^2 < distance * distance
	 * fin fonction
	 */
	/**** fonction determine s'ils sont voisin *****/
	/****************************************************************/
	protected static boolean sontVoisin(Table a, Table b, int distance)

	{

		// verifier si a et >b
		long distance_a_b = distanceSquare(a, b);
		return distance_a_b < distance * distance && a.id != b.id;

	}

	public static long pow(long a) {
		return a * a;
	}

	/*********************************************************
	 * fonction intersectionne(_xmin, _xmax, _ymin, _ymax) retourner non (xmax <
	 * _xmin ou _xmax < xmin ou ymax < _ymin ou _ymax < ymin) fin fonction
	 * 
	 */
	/******************* INTERSECTION **************************/
	/********************************************************/
	protected boolean intersection(int _xmin, int _xmax, int _ymin, int _ymax) {
		return !(this.xmax < _xmin || _xmax < this.xmin || this.ymax < _ymin || _ymax < this.ymin);
	}

	/****************************************************************
	 * fonction trouveVoisin(_xmin, _xmax, _ymin, _ymax, distance, Table t) si
	 * intersectionne(_xmin, _xmax, _ymin, _ymax) alors si table est d�fini alors si
	 * sontVoisin(t, table, distance) alors t.voisins.add(table.id) fin si fin si si
	 * non feuille alors xiyi.trouveVoisin(_xmin, _xmax, _ymin, _ymax, distance, t)
	 * xiys.trouveVoisin(_xmin, _xmax, _ymin, _ymax, distance, t)
	 * xsyi.trouveVoisin(_xmin, _xmax, _ymin, _ymax, distance, t)
	 * xsys.trouveVoisin(_xmin, _xmax, _ymin, _ymax, distance, t) fin si fin si fin
	 * fonction
	 */

	public void trouverVoisinStack(int _xmin, int _xmax, int _ymin, int _ymax, int distance, Table t) {
		if (intersection(_xmin, _xmax, _ymin, _ymax)) {
			if (this.table != null) {
				if (sontVoisin(t, this.table, distance)) {
					t.voisins.add(this.table.id);
				}
			}

			if (!(this.feuille)) {
				this.xiyi.trouverVoisinStack(_xmin, _xmax, _ymin, _ymax, distance, t);
				this.xiys.trouverVoisinStack(_xmin, _xmax, _ymin, _ymax, distance, t);
				this.xsyi.trouverVoisinStack(_xmin, _xmax, _ymin, _ymax, distance, t);
				this.xsys.trouverVoisinStack(_xmin, _xmax, _ymin, _ymax, distance, t);
			}
		} else if (this.table != null) {
			if (sontVoisin(t, table, distance)) {
				String error = "NOT ACCEPTED BUT SHOULD\n"
						+ "in [[" + _xmin + "," + _xmax + "],["+ _ymin + "," + _ymax + "]]\n"
						+ "this [[" + xmin + "," + xmax + "],["+ ymin + "," + ymax + "]]\n"
						+ "Distance: " + distanceSquare(this.table, t) + " < " + distance +"\n"
						+ this.table + " - " + t;
				new Exception(error).printStackTrace();
			}
		}
	}

	public void trouverVoisinHeap(int _xmin, int _xmax, int _ymin, int _ymax, int distance, Table t) {
		ArrayDeque<Noeud> stack = new ArrayDeque<>();
		stack.addLast(this);
		while (!stack.isEmpty()) {
			Noeud current = stack.removeLast();
			if (current.intersection(_xmin, _xmax, _ymin, _ymax)) {
				if (current.table != null) {
					if (sontVoisin(t, current.table, distance)) {
						t.voisins.add(current.table.id);
					}
				}
				if (!(current.feuille)) {
					stack.addLast(current.xiyi);
					stack.addLast(current.xiys);
					stack.addLast(current.xsyi);
					stack.addLast(current.xsys);
				}
			}
		}
	}
}
