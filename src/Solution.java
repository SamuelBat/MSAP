import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class Solution implements Comparable<Solution> {
	public HashMap<Integer, Integer> etats = new HashMap<>();

	public int selected() {
		return (int) etats.values().stream().filter(i -> i == 1).count();
	}

	public int get(int id) {
		Integer v = etats.get(id);
		return v == null ? -1 : v;
	}

	public int get(Table t) {
		return get(t.id);
	}

	public void set(int id, int v) {
		etats.put(id, v);
	}

	public void set(Table t, int v) {
		set(t.id, v);
	}

	private boolean checkVoisins(Stream<Table> stream) {
		return stream.filter(t -> get(t) == 1)
				.allMatch(t -> t.voisins.stream()
						.noneMatch(tv -> get(tv) == 1));
	}

	public boolean checkSubSolution(Set<Table> tables) {
		return checkVoisins(tables.stream());

	}

	public boolean checkFullSolution(List<Table> tables) {
		return tables.stream().noneMatch(t -> get(t) == -1) && checkVoisins(tables.stream());
	}

	public static Solution max(Solution a, Solution b) {
		return a.compareTo(b) < 0 ? b : a;
	}

	public boolean isBetterThan(Solution o) {
		return selected() > o.selected();
	}

	@Override
	public int compareTo(Solution o) {
		return Integer.compare(selected(), o.selected());
	}

	@Override
	public String toString() {
		return "{Max=" + selected() + " values= " + etats.toString() + "}";
	}

	public Solution add(Solution other) {
		other.etats.forEach((id, etat) -> {
			etats.compute(id, (k, old) -> {
				if (old == null) {
					return etat;
				} else if (old != etat && old != -1 && etat != -1) {
					throw new ConcurrentModificationException("For id: " + id + " old value: " + old + " is different from new value: " + etat);
				} else {
					return etat;
				}
			});
		});
		return this;
	}

	public int size() {
		return etats.size();
	}
}
