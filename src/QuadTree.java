import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class QuadTree extends Noeud {

	private boolean _manual_stack = false;

	public QuadTree(int xmin, int xmax, int ymin, int ymax) {
		super(xmin, xmax, ymin, ymax);

	}

	public void trouverVoisinStack(Table t, int distance) {
		trouverVoisinStack(t.x - distance, t.x + distance, t.y - distance, t.y + distance, distance, t);
	}

	public void trouverVoisinHeap(Table t, int distance) {
		trouverVoisinHeap(t.x - distance, t.x + distance, t.y - distance, t.y + distance, distance, t);
	}

	public void trouverVoisinsAuto(Table t, int distance) {
		if (_manual_stack) {
			trouverVoisinHeap(t, distance);
		} else {
			try {
				trouverVoisinStack(t, distance);
			} catch (StackOverflowError e) {
				System.err.println(e.toString() + " -> Switch to heap.");
				t.voisins.clear();
				_manual_stack = true;
				trouverVoisinHeap(t, distance);
			}
		}
	}

	private static void perftest() {
		int distance = 100;
		int debug = 0;
		for (int i = 1000; i <= 32000; i *= 2) {
			List<Table> tables = new ArrayList<>(), tables2 = new ArrayList<>(), tables3 = new ArrayList<>();
			Random r = new Random(0);
			int[] xmax = { 0 }, ymax = { 0 };

			IntStream.range(1, i).forEach(j -> {
				int x = r.nextInt(1000), y = r.nextInt(1000);
				tables.add(new Table(j, x, y, new ArrayList<>()));
				tables2.add(new Table(j, x, y, new ArrayList<>()));
				tables3.add(new Table(j, x, y, new ArrayList<>()));
				xmax[0] = Math.max(xmax[0], x);
				ymax[0] = Math.max(ymax[0], y);
			});

			Instant start1 = Instant.now();
			tables.stream()
					.forEach(t1 -> {
						tables.stream()
								.filter(t2 -> t2.id != t1.id)
								.forEach(t2 -> {
									int dist = (t1.x - t2.x) * (t1.x - t2.x) + (t1.y - t2.y) * (t1.y - t2.y);
									if (dist < distance * distance) {
										t1.voisins.add(t2.id);
									}
								});
					});
			Instant stop1 = Instant.now();

			// System.out.println(tables.toString().substring(0, 100));

			Instant start2 = Instant.now();
			QuadTree qt = new QuadTree(0, xmax[0], 0, ymax[0]);
			tables2.forEach(qt::addTable);
			try {
				tables2.forEach(t -> qt.trouverVoisinStack(t, distance));
			} catch (StackOverflowError e) {
				System.err.println(e.toString());
			}

			Instant stop2 = Instant.now();
			// System.out.println(tables2.toString().substring(0, 100));
			int count = 0;
			for (int j = 0; j < tables.size(); j++) {
				Table t1 = tables.get(j);
				Table t2 = tables2.get(j);
				if (t1.voisins.size() != t2.voisins.size()) {
					count++;
					if (count < debug) {
						System.out.println(t1);
						System.out.println(t2);
						System.out.println("---");
					}
				}
			}

			Instant start3 = Instant.now();
			QuadTree qt2 = new QuadTree(0, xmax[0], 0, ymax[0]);
			tables3.forEach(qt2::addTable);
			tables3.forEach(t -> qt2.trouverVoisinsAuto(t, distance));
			Instant stop3 = Instant.now();
			// System.out.println(tables2.toString().substring(0, 100));
			int count2 = 0;
			for (int j = 0; j < tables.size(); j++) {
				Table t1 = tables.get(j);
				Table t3 = tables3.get(j);
				if (t1.voisins.size() != t3.voisins.size()) {
					count2++;
					if (count2 < debug) {
						System.out.println(t1);
						System.out.println(t3);
						System.out.println("---");
					}
				}
			}

			System.out.println(i + ": " +
					Duration.between(start1, stop1).toMillis() + "ms vs " + Duration.between(start2, stop2).toMillis()
					+ "ms vs " + Duration.between(start3, stop3).toMillis()
					+ "ms (" + count + ", " + count2 + " Errors)");
		}
	}
	
	private static void test() {
		int distance = 100;
		int debug = 10;
		List<Table> tables = new ArrayList<>(), tables2 = new ArrayList<>();
		Random r = new Random(0);
		int[] xmax = { 0 }, ymax = { 0 };

		IntStream.range(1, 10000).forEach(j -> {
			int x = r.nextInt(1000), y = r.nextInt(1000);
			tables.add(new Table(j, x, y, new ArrayList<>()));
			tables2.add(new Table(j, x, y, new ArrayList<>()));
			xmax[0] = Math.max(xmax[0], x);
			ymax[0] = Math.max(ymax[0], y);
		});

		Instant start1 = Instant.now();
		tables.stream()
				.forEach(t1 -> {
					tables.stream()
							.filter(t2 -> t2.id != t1.id)
							.forEach(t2 -> {
								int dist = (t1.x - t2.x) * (t1.x - t2.x) + (t1.y - t2.y) * (t1.y - t2.y);
								if (dist < distance * distance) {
									t1.voisins.add(t2.id);
								}
							});
				});
		Instant stop1 = Instant.now();

		// System.out.println(tables.toString().substring(0, 100));

		Instant start2 = Instant.now();
		QuadTree qt = new QuadTree(0, xmax[0], 0, ymax[0]);
		tables2.forEach(qt::addTable);
		try {
			tables2.forEach(t -> qt.trouverVoisinStack(t, distance));
		} catch (StackOverflowError e) {
			System.err.println(e.toString());
		}

		Instant stop2 = Instant.now();
		// System.out.println(tables2.toString().substring(0, 100));
		int count = 0;
		for (int j = 0; j < tables.size(); j++) {
			Table t1 = tables.get(j);
			Table t2 = tables2.get(j);
			if (t1.voisins.size() != t2.voisins.size()) {
				count++;
				if (count < debug) {
					System.out.println(t1);
					System.out.println(t2);
					System.out.println("---");
				}
			}
		}

		System.out.println(
				Duration.between(start1, stop1).toMillis() + "ms vs " + Duration.between(start2, stop2).toMillis()
						+ "ms (" + count + " Errors)");
	}

	public static void main(String[] args) {
		//testAdd();
		//perftest();
		//test();
	}
}
