import java.util.List;
import java.util.Set;

/**
 * 
 *
 * @author Alexis Vernes
 *
 */
public class QuickSolver implements Solver {

	@Override
	public Solution _solve(Set<Table> patch, List<Table> tables, int nbVoisinMax) {
		Solution solution = new Solution();
		patch.stream()
				.sorted((a, b) -> a.voisins.size() - b.voisins.size())
				.forEach(t -> {
					if (solution.get(t) == -1) {
						solution.set(t, 1); // select
						for (Integer voisin : t.voisins) {
							solution.set(voisin, 0); // deselect neibourg
						}
					}
					// System.out.println(t);
				});
		return solution;
	}
}
