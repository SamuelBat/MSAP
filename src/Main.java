import java.security.InvalidParameterException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Set;

public class Main {

	public static String format(Duration duration) {
		if (duration.toMinutes() == 0) {
			if (duration.toSeconds() == 0) {
				long nano = duration.toNanos();
				if (nano < 1_000_000) {
					return nano + "ns";
				} else {
					String n = ("." + (nano % 1_000_000));
					return (nano / 1_000_000) + (n.length() < 5 ? n : n.substring(0, 4)) + "ms";
				}
			} else {
				return duration.toSeconds() + "." + duration.toMillisPart() + "s";
			}
		} else {
			return duration.toSeconds() + "s";
		}
	}

	public static void main(String[] args) throws Exception {

		Duration timeout = Duration.ofSeconds(10);
		int no_improvement_max = 10000;
		String file = "file/instance.txt";
		int distance = 100;
		boolean isRandom = false;
		boolean hide = false;
		int maxX = 0, maxY = 0, dist = 0, shiftMax = 0;
		double density = 0.0;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-d") && i < args.length - 1) {
				distance = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-l") && i < args.length - 1) {
				no_improvement_max = Integer.parseInt(args[++i]);
			} else if (args[i].equals("--nl")) {
				no_improvement_max = -1;
			} else if (args[i].equals("--f")) {
				no_improvement_max = 0;
			} else if (args[i].equals("--h")) {
				hide = true;
			} else if (args[i].equals("-t") && i < args.length - 1) {
				timeout = Duration.ofSeconds(Integer.parseInt(args[++i]));
			} else if (args[i].equals("-r") && i < args.length - 1) {
				isRandom = true;
				String[] params = args[++i].split(",");
				if (params.length != 5) {
					throw new InvalidParameterException("-r required 5 args: maxX,maxY,dist,shiftMax,density got: " + args[i]);
				}
				maxX = Integer.parseInt(params[0]);
				maxY = Integer.parseInt(params[1]);
				dist = Integer.parseInt(params[2]);
				shiftMax = Integer.parseInt(params[3]);
				density = Double.parseDouble(params[4]);
			} else if (args[i].startsWith("-")) {
				throw new InvalidParameterException(args[i]);
			} else {
				file = args[i];
			}
		}

		System.out.println("Timeout: " + timeout.toSeconds() + "s");
		if (no_improvement_max >= 0) {
			System.out.println(no_improvement_max == 0 ? "Return first solution"
					: "Iteration max without improvement: " + no_improvement_max);
		}
		System.out.println("Distance: " + distance + "cm");
		if (isRandom) {
			System.out.println("Random tables: size=[" + maxX + "," + maxY+ "] dist=" + dist + " shiftMax=" + shiftMax + " density=" + density);
		} else {
			System.out.println("File: " + file);
		}
		
		System.out.println("Initializing...");
		Instant global_start = Instant.now();
		Instant start = global_start;
		Model model = new Model();

		ArrayList<Set<Table>> patches = new ArrayList<>();
		ArrayList<Table> tables = new ArrayList<>();
		int max;
		if (isRandom) {
			max = model.generateRandom(maxX, maxY, dist, shiftMax, density, tables, patches, distance);
		} else {
			max = model.generateFromFile(file, tables, patches, distance);
		}
		System.out.println("Voisin max: " + max);
		patches.forEach(t -> {
			System.out.println(t);
			if (t.size() == 1) {
				t.forEach(tt -> System.out.println("\t" + tt.getVoisins()));
			}
		});

		Instant stop = Instant.now();
		System.out.println("Initialized in " + format(Duration.between(start, stop)));
		System.out.println();
		System.out.println("Solving...");
		start = Instant.now();

		Solver solver = new SortingSolver();
		Solution global = new Solution();
		int i = 1;
		for (Set<Table> patch : patches) {
			Solution local = solver.solve_while(patch, tables, max, timeout, no_improvement_max);
			if (!local.checkSubSolution(patch)) {
				throw new Exception("Local solution not valid !");
			}
			global.add(local);
			System.out.println("\t" + (i++) + "/" + patches.size() + "Done.");
		}
		if (!global.checkFullSolution(tables)) {
			throw new Exception("Global solution not valid !");
		}

		stop = Instant.now();
		Instant global_stop = stop;
		System.out
				.println("\nFinal solution solved in " + format(Duration.between(start, stop)) + " with a distance of "
						+ distance + ":");
		if (hide) {
			System.out.println(global.selected() + "/" + global.size() + " tables selected.");
		} else {
			System.out.println(global);
		}
		System.out.println();
		System.out.println("Total time: " + format(Duration.between(global_start, global_stop)));
	}

}
