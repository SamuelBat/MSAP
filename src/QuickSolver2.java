import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Use a arraylist of arraylist to improve performance vs sorting at every step
 *
 * @author Alexis Vernes
 *
 */
public class QuickSolver2 implements Solver {

	@Override
	public Solution _solve(Set<Table> patch, List<Table> tables, int nbVoisinMax) {
		Solution solution = new Solution();
		ArrayList<Table>[] sort = new ArrayList[nbVoisinMax + 1];
		for (int i = 0; i < sort.length; i++) {
			sort[i] = new ArrayList<>();
		}
		patch.forEach(t->sort[t.voisins.size()].add(t));
		
		for (int i = 0; i < sort.length; i++) {
			Collections.shuffle(sort[i]);
			sort[i].stream().forEach(t -> {
				if (solution.get(t) == -1) {
					solution.set(t, 1);
					for (Integer voisin : t.voisins) {
						solution.set(voisin, 0);
					}
				}
				// System.out.println(t);
			});
		}

		return solution;
	}
}
