import java.util.ArrayList;

/***
 * 
 * @author samuel
 *
 */
public class Table {

	int id;
	int x, y;
	ArrayList<Integer> voisins;

	public Table() {

	}

	public Table(int id, int x, int y, ArrayList<Integer> voisins) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.voisins = voisins;
	}
	
	public void addNeighbor(int _id) {
		voisins.add(_id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public ArrayList<Integer> getVoisins() {
		return voisins;
	}

	public void setVoisins(ArrayList<Integer> voisins) {
		this.voisins = voisins;
	}

	@Override
	public String toString() {
		return "{ id=" + id + " xy=[" + x + "," + y + "] count=" + voisins.size() + "}";
	}
}
