import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 
 *
 * @author Alexis Vernes
 *
 */
public interface Solver {
	public static ThreadLocalRandom RANDOM = ThreadLocalRandom.current();

	/**
	 * 
	 *
	 * @param patch
	 * @param tables
	 * @param nbVoisinMax
	 * @return One of the best solution
	 */
	public Solution _solve(Set<Table> patch, List<Table> tables, int nbVoisinMax);

	/**
	 * Print to console.
	 * 
	 * @see _solve
	 */
	public default Solution solve(Set<Table> patch, List<Table> tables, int nbVoisinMax) {
		System.out.println("Using " + getClass().getSimpleName() + " to start solving solution with "
				+ patch.size() + " tables.");
		Instant start = Instant.now();
		Solution solution = _solve(patch, tables, nbVoisinMax);
		Instant stop = Instant.now();
		System.out.println("\tSolved in " + Main.format(Duration.between(start, stop)) + ", " + solution.selected() + " selected.");
		return solution;
	}

	/**
	 * Solve until timeout or no improvement.
	 *
	 * @param patch
	 * @param tables
	 * @param nbVoisinMax
	 * @param timeout
	 * @param no_improvement_max
	 * @return
	 */
	public default Solution solve_while(Set<Table> patch, List<Table> tables, int nbVoisinMax, Duration timeout, long no_improvement_max) {
		System.out.println("Using " + getClass().getSimpleName() + " to start solving solution with " + patch.size() + " tables.");
		Instant start = Instant.now();
		Instant to = start.plus(timeout);
		Solution max = new Solution();
		boolean is_timeout = Instant.now().isAfter(to);
		long iteration = 0;
		long no_improvement = 0;
		while ((no_improvement_max < 0 || iteration == 0 || no_improvement < no_improvement_max) && !is_timeout) {
			Solution sol = _solve(patch, tables, nbVoisinMax);
			if(sol.isBetterThan(max)) {
				max = sol;
				System.out.println("New count: " + max.selected() + " after " + (no_improvement + 1) + " iterations");
				no_improvement = 0;
			} else {
				no_improvement++;
			}
			is_timeout = Instant.now().isAfter(to);
			iteration++;
		}
		Instant stop = Instant.now();
		System.out.println("\tSolved in " + Main.format(Duration.between(start, stop)) + (is_timeout ? " (TIMEOUT), " : ", ") + max.selected() + " selected, " + iteration + " iterations.");
		return max;
	}

	/*
	 * TEST
	 */
	
	
	public static void test(int count, int voisin, int[] scores) {
		ArrayList<Table> all[] = new ArrayList[3];
		HashSet<Table> set[] = new HashSet[3];
		for (int i = 0; i < set.length; i++) {
			all[i] = new ArrayList<>();
			set[i] = new HashSet<>();
		}
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < set.length; j++) {
				Table t = new Table(i + 1, 0, 0, new ArrayList<>());
				all[j].add(t);
				set[j].add(t);
			}
		}

		for (int id1 = 0; id1 < count; id1++) {
			int voisins = RANDOM.nextInt(voisin);
			for (int j = 0; j < voisins; j++) {
				int id2 = RANDOM.nextInt(count);
				if (id1 != id2 && !all[0].get(id1).voisins.contains(id2 + 1)) {
					for (int i = 0; i < set.length; i++) {
						all[i].get(id1).voisins.add(id2 + 1);
						all[i].get(id2).voisins.add(id1 + 1);
					}
				}
			}
		}

		int max = all[0].stream().map(t -> t.voisins.size()).max(Integer::compare).get();
		System.out.println("vmax " + max + " calculated " + (voisin * 2 + 1));
		int voisin_max = voisin * 2 + 10;
		Solution[] sol = new Solution[3];
		Duration timeout = Duration.ofSeconds(10);
		int no_improvement_max = -1;
		sol[0] = new QuickSolver().solve_while(set[0], all[0], voisin_max, timeout, no_improvement_max);
		sol[1] = new QuickSolver2().solve_while(set[1], all[1], voisin_max, timeout, no_improvement_max);
		sol[2] = new SortingSolver().solve_while(set[2], all[2], voisin_max, timeout, no_improvement_max);
		int imax = 0;
		int scmax = sol[imax].selected();
		for (int i = 1; i < sol.length; i++) {
			int sc = sol[i].selected();
			if (scmax < sc) {
				imax = i;
				scmax = sc;
			}
		}
		for (int i = 0; i < sol.length; i++) {
			if (!sol[i].checkFullSolution(all[i])) {
				System.err.println("Error solution " + i + " not valid.");
			}
		}
		scores[imax]++;
		System.out.println("Max:" + sol[imax].selected());

		System.out.println("\ntest done.");
	}

	public static void main(String[] args) {
		int[] scores = new int[3];
		for (int i = 0; i < 5; i++) {
			test(10000, 100, scores);
		}
		System.out.println(Arrays.toString(scores));
	}
}
