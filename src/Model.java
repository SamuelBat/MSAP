import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 
 * @author samuel
 *
 */
public class Model {
	QuadTree baseNode;
	int xMax, yMax;

	public Model() {

	}
	
	public void check(List<Table> tables) {
		for (Table table : tables) {
			for (int id : table.voisins) {
				Table v = tables.get(id - 1);
				if (!v.voisins.contains(table.id)) {
					System.err.println("Error:");
					System.err.println(table);
					System.err.println(table.voisins);
					System.err.println(v);
					System.err.println(v.voisins);
				}
			}
		}
	}

	public int generateRandom(int maxX, int maxY, int dist, int shiftMax, double density, final ArrayList<Table> tables, final ArrayList<Set<Table>> patchs, int distance) {
		ThreadLocalRandom r = ThreadLocalRandom.current();
		int id = 1;
		xMax = Integer.MIN_VALUE;
		yMax = Integer.MIN_VALUE;
		for (int x = shiftMax; x < maxX - shiftMax; x += dist) {
			for (int y = shiftMax; y < maxY - shiftMax; y+= dist) {
				if (r.nextDouble() < density) {
					int vx = x + (shiftMax < 1 ? 0 : r.nextInt(-shiftMax, shiftMax));
					int vy = y + (shiftMax < 1 ? 0 : r.nextInt(-shiftMax, shiftMax));
					tables.add(new Table(id++, vx, vy, new ArrayList<>()));
					xMax = Math.max(vx, xMax);
					yMax = Math.max(vy, yMax);
				}
			}
		}
				
		return generate(tables, patchs, distance);
	}
	
	public int generateFromFile(String src, final ArrayList<Table> tables, final ArrayList<Set<Table>> patchs, int distance) {
		try {
			read(src, tables);
			return generate(tables, patchs, distance);
		} catch (IOException e) {
			e.printStackTrace();
			tables.clear();
			patchs.clear();
			return 0;
		}
	}
	
	private int generate(final ArrayList<Table> tables, final ArrayList<Set<Table>> patchs, int distance) {
		System.out.println(tables.size() + " tables recupere");
		
		baseNode = new QuadTree(-10, xMax+10, -10, yMax+10);
		tablesToTree(tables);
				
		return generatePatch(tables, patchs, distance);
	}

	private void tablesToTree(List<Table> table) {
		table.forEach(t -> {
			//System.out.println(t);
			baseNode.addTable(t);
		});
	}

	private void read(String src, final ArrayList<Table> tables) throws IOException {
		BufferedReader reader;

		reader = new BufferedReader(new FileReader(src));
		xMax = Integer.MIN_VALUE;
		yMax = Integer.MIN_VALUE;
		reader.readLine();
		while (reader.ready()) {
			String[] tmp = reader.readLine().split("\t");
			int x = Integer.parseInt(tmp[1]);
			int y = Integer.parseInt(tmp[2]);
			tables.add(new Table(Integer.parseInt(tmp[0]), x, y, new ArrayList<>()));

			xMax = Math.max(x, xMax);
			yMax = Math.max(y, yMax);
		}

		reader.close();
	}

	public int generatePatch(final List<Table> tables, final List<Set<Table>> patchs, int distance) {
		System.out.println("xMax vaut " + xMax + "\nyMax vaut " + yMax);
		System.out.println("Debut de la recherche de voisins");
		HashSet<Table> nonDecouvert = new HashSet<>(tables);
		Set<Table> nonExp = new HashSet<>();
		Set<Table> exp = new HashSet<>();
		int maxVoisins = Integer.MIN_VALUE;

		tables.forEach(t->{
			baseNode.trouverVoisinsAuto(t, distance);
		});
		
		//check(tables);
		
		while (!nonDecouvert.isEmpty()) {
			Table tmp = nonDecouvert.iterator().next();
			nonExp.add(tmp);
			nonDecouvert.remove(tmp);
			while (!nonExp.isEmpty()) {
				Table table = nonExp.iterator().next();
				//table.etat = 0;
				nonExp.remove(table);

				exp.add(table);
				
				table.getVoisins().stream().map(t ->tables.get(t-1)).filter(t->nonDecouvert.contains(t)).forEach(t-> nonExp.add(t));
				
				nonDecouvert.removeAll(nonExp);
				maxVoisins = Math.max(table.getVoisins().size(), maxVoisins);

			}
			patchs.add(exp);
			exp = new HashSet<>();

		}
		System.out.println("Tous les voisins ont �t� trouv�");
		return maxVoisins;
	}
}
