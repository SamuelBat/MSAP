
# The Maximum Seats Allocation Problem

## Format de fichier d'instance

	seat x y
	1	0	0
	2	0	75
	3	0	150
	etc...

La coordonnée minimale acceptée et supposée des tables est 0,0.
L'indice des tables doit aller de 1 à n inclus sans valeurs manquantes.

## Compiler les sources

	javac -d .\bin\ -sourcepath .\src\ .\src\Main.java

## Lancer le programme

### Commande

	java -cp .\bin\ Main [-d <DISTANCE>] [--h] [-l <TENTATIVE_MAX> | --nl | --f] [-t <TIMEOUT>] [-r <MAX_X,MAX_Y,DISTANCE,SHIFT_MAX,DENSITY>] [<FILE>]
Arguments:
- -d DISTANCE : Distance sociale en cm (défaut = 100)
- --h : N'affiche pas la solution maximale
- -l TENTATIVE_MAX : Tentative maximale pour améliorer la solution du sous problème (défaut = 10000)
- --nl : Cherche d'autres solutions au sous problème jusqu'à atteindre le timeout
- --f : Retourne la première solution au sous problème
- -t TIMEOUT : Temps maximum en secondes de résolution d'un sous problème (défaut = 10s)
- -r MAX_X,MAX_Y,DISTANCE,SHIFT_MAX,DENSITY : Génération aléatoire des tables
	- MAX_X : Emplacement max
	- MAX_Y : Emplacement max
	- DISTANCE : Distance entre 2 tables
	- SHIFT_MAX : Déplacement aléatoire
	- DENSITY : Quantité de table à générer
- FILE : Fichier définissant l'emplacement des tables (défaut = file/instance.txt)

### Exemples:

	java -cp .\bin\ Main
	java -cp .\bin\ Main -d 200
	java -cp .\bin\ Main --f .\file\instance.txt
	java -cp .\bin\ Main -r 1000,1000,100,10,0.7 -d 100
	java -cp .\bin\ Main -t 1 --nl .\file\instance.txt